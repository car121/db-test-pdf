﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void categoriesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.categoriesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.uTNGDBDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'uTNGDBDataSet.products' Puede moverla o quitarla según sea necesario.
            this.productsTableAdapter.Fill(this.uTNGDBDataSet.products);
            // TODO: esta línea de código carga datos en la tabla 'uTNGDBDataSet.categories' Puede moverla o quitarla según sea necesario.
            this.categoriesTableAdapter.Fill(this.uTNGDBDataSet.categories);

        }
    }
}
